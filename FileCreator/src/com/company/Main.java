package com.company;

import java.io.*;
import java.math.BigInteger;
import java.util.Random;

public class Main {

    public static void main(String[] args) throws IOException {
	// write your code here
        BigInteger id = BigInteger.valueOf(1);
        BigInteger increment = BigInteger.valueOf(1);
        Random random = new Random();
        char[] alphabets = {'a','b','c','d','e','f','g','h','i','j','j','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
        String fileName = "C:\\Users\\amabhard\\Desktop\\data.txt";
        File file = new File("filename");
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
        writer.write("Id\t\tName\t\t\tCountry");
        writer.newLine();
        writer.newLine();
        while(file.length()/(1024*1024*1024)<4)
        {
            writer.write(id.toString() + "\t\t");
            int start = 0;
            int range = random.nextInt(4)+4;
            //System.out.println(range);
            while(start<range)
            {
                writer.write(alphabets[random.nextInt(alphabets.length)]);
                start++;
            }
            writer.write("\t\t\t");
            writer.write("India");
            writer.newLine();
            id = id.add(increment);
        }
    }
}
