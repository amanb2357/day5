package com.company;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Queries {

    Connection connection;
    public Queries() {
        this.connection = DBConnect.getConnection();
    }

    public void SelectAll() throws SQLException {

        Statement statement = connection.createStatement();

        ResultSet resultSet = statement.executeQuery("select * from players");

        while(resultSet.next())
            System.out.println(resultSet.getInt(1)+"  "+resultSet.getString(2)+"  "+resultSet.getString(3));

        connection.close();
    }

    public void Insert(String query) throws SQLException {

        Statement statement = connection.createStatement();

        statement.executeQuery(query);

        //while(resultSet.next())
        //    System.out.println(resultSet.getInt(1)+"  "+resultSet.getString(2)+"  "+resultSet.getString(3));
        //System.out.println(resultSet.toString());

        connection.close();
    }

    public void Delete(String query) throws SQLException {

        Statement statement = connection.createStatement();

        statement.executeQuery(query);

        //while(resultSet.next())
        //    System.out.println(resultSet.getInt(1)+"  "+resultSet.getString(2)+"  "+resultSet.getString(3));
        //System.out.println(resultSet.toString());

        connection.close();
    }

    public void Update(String query) throws SQLException {

        Statement statement = connection.createStatement();

        statement.executeQuery(query);

        //while(resultSet.next())
        //    System.out.println(resultSet.getInt(1)+"  "+resultSet.getString(2)+"  "+resultSet.getString(3));
        //System.out.println(resultSet.toString());

        connection.close();
    }
}
